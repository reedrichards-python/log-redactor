============
Log Redactor
============


.. image:: https://img.shields.io/pypi/v/log_redactor.svg
        :target: https://pypi.python.org/pypi/log_redactor

.. image:: https://img.shields.io/travis/reedrichards/log_redactor.svg
        :target: https://travis-ci.org/reedrichards/log_redactor

.. image:: https://readthedocs.org/projects/log-redactor/badge/?version=latest
        :target: https://log-redactor.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Remove a pattern from your logs


* Free software: MIT license
* Documentation: https://log-redactor.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
