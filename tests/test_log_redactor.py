#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `log_redactor` package."""

import pytest
import logging


from log_redactor.formatters import RedactingFormatter


def test_pattern_removed(capture):

    for h in logging.root.handlers:
            h.setFormatter(RedactingFormatter(orig_formatter=h.formatter, patterns=['hide me']))
    logger = logging.getLogger()

    logger.info('test hide me')
    logger.debug('test hide me')
    logger.error('hide me')

    capture.check(
        ('root', 'INFO', 'test YEET'),
        ('root', 'DEBUG', 'test YEET'),
        ('root', 'ERROR', 'YEET'),
    )
