# -*- coding: utf-8 -*-

"""Top-level package for Log Redactor."""

__author__ = """Robert Wendt"""
__email__ = 'rwendt1337@gmail.com'
__version__ = '0.1.1'
